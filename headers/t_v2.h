/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_v2.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: snicolet <snicolet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/26 20:56:01 by snicolet          #+#    #+#             */
/*   Updated: 2016/08/23 18:37:19 by snicolet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef T_V2_H
# define T_V2_H

typedef struct		s_v2f
{
	float			x;
	float			y;
}					t_v2f;

typedef struct		s_v2i
{
	int				x;
	int				y;
}					t_v2i;

typedef struct		s_v2ui
{
	unsigned int	x;
	unsigned int	y;
}					t_v2ui;

#endif
