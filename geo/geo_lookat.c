/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   geo_lookat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: snicolet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/28 03:59:35 by snicolet          #+#    #+#             */
/*   Updated: 2017/05/30 01:43:05 by snicolet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "geo.h"

/*
** returns a matrix to look at a position
** r = the right vector
** u = the up vector
** d = direction vector
*/

t_m4		geo_lookat(const t_v4d r, const t_v4d u, const t_v4d d)
{
	return ((t_m4){
		.x = (t_v4d){r.x, r.y, r.z, 0.0},
		.y = (t_v4d){u.x, u.y, u.z, 0.0},
		.z = (t_v4d){d.x, d.y, d.z, 0.0},
		.w = (t_v4d){0.0, 0.0, 0.0, 1.0}
	});
}
